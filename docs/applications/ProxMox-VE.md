# Proxmox VE

## Creating an OS template

1. Get the latest image

For Ubuntu at the time of writing: https://cloud-images.ubuntu.com/jammy/current/

```
wget https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img
```

2. Convert the image to RAW

```
qemu-img convert -f qcow2 -O raw focal-server-cloudimg-amd64.img focal-server-cloudimg-amd64.raw
```

3. Create the VM Template

```
qm create 9002 —-agent enabled=1
```

4. Import the disk
```
qm importdisk 9002 focal-server-cloudimg-amd64.raw data
```

5. Make the disk bigger

For ceph:

```
rbd resize data/vm-9002-disk-0 --size 10G
```

For ZFS:

```
qm resize 9002 virtio0 10G
```

6. Set all the options
You'll want to update the --name and --scsi0 disk name

With ceph:
```
qm set 9002 --name template-2022-05-10-ubuntu-22.04 --scsihw virtio-scsi-single --scsi0 data:vm-9002-disk-0,discard=on,iothread=1,ssd=1 --ide2 data:cloudinit --boot c --bootdisk scsi0 --net0 virtio,bridge=vmbr0 --protection 1 --ostype l26 --cpu host --memory 1024 --cores 2 --sockets 1 --numa 1 --serial0 socket --vga serial0 --hotplug disk,network,usb,memory,cpu
```

With ZFS:

```
qm set 9002 --name template-2022-10-25-ubuntu-22.04 --ide2 OneTs:cloudinit --boot c --bootdisk virtio0 --net0 virtio,bridge=vmbr0 --protection 1 --ostype l26 --cpu host --memory 1024 --cores 2 --sockets 1 --numa 1 --serial0 socket --vga serial0 --hotplug disk,network,usb,memory,cpu
```

7. Set as a template
```
qm template 9002
```
