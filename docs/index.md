# cat notes | grep xyz



cat notes collates the sacred texts from the secret notes of engineers. 

Aim: All good engineers/sysadmins/wizards have their notes. Whether they're scribbled in an analog journal or self-hosted on a server running in a garage. cat notes wants to share this collective knowledge and make it accessible to others. Hopefully one of the commands found here will save you from scrolling to the tenth page of Google hunting for the perfect answer.  

strictly open source 