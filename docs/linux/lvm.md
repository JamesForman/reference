# LVM

## Physical Volumes (PVs)

### Creating a Physical Volume

```
pvcreate /dev/mapper/extBackup
```

### Expanding a Physical Volume
```
sudo pvresize /dev/mapper/Array0
```

### Removing a PV
```
sudo pvremove /dev/vdd
```

### Potential Issues

#### Device /dev/X not found (or ignored by filtering)
```
$ sudo pvcreate /dev/sdb
  Device /dev/sdb not found (or ignored by filtering).

# dd if=/dev/zero of=/dev/sdb
^C845369+0 records in
845369+0 records out
432828928 bytes (433 MB, 413 MiB) copied, 9.88539 s, 43.8 MB/s

# pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created
```

## Volume Groups (VGs)

### Creating a Volume Group
```
vgcreate extBackup1 /dev/mapper/extBackup
```

### Removing a VG
```
sudo vgremove raspbian
```

### Renaming a VG
```
sudo vgrename /dev/oldname /dev/newname
```

### Moving a VG to another PV
```
Add new disk (xvdf)

pvcreate /dev/xvdf

vgextend vg0 /dev/xvdf

pvmove /dev/xvdb /dev/xvdf

vgreduce vg0 /dev/xvdb
```

#### Aborting
```
pvmove --abort

vgreduce vg0 /dev/xvdf
```

## Logical Volumes (LVs)

### Creating a Logical Volume
```
lvcreate -L 10737418240b -n JFNZWGPCAT01 512SSD
lvcreate -L 10G -n /dev/512SSD/Swap
```

### Expanding a Logical Volume
```
sudo lvextend --size +2T /dev/Array0_Decrypted/Array0_Data

sudo lvextend -l +100%FREE /dev/Array0_Decrypted/Array0_Data

sudo lvextend -l +6 /dev/mapper/vg0-home [Extend by extents]
```

### Removing an LV
```
sudo lvremove /dev/mapper/ubuntu-whatever-lv
```

### Locate a LV
In the below example the Segments show the LV is on /dev/vda. Useful for multi-PV LVs.

```
# lvdisplay --maps
  --- Logical volume ---
  LV Name                /dev/vg0/srv
  VG Name                vg0
  LV UUID                c9hbjS-ui4Y-iVLw-anQ3-wkWm-0rMd-RVaaWk
  LV Write Access        read/write
  LV Status              available
  # open                 1
  LV Size                22.00 GiB
  Current LE             5631
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           251:3
   
  --- Segments ---
  Logical extent 0 to 5630:
    Type                linear
    Physical volume     /dev/vda
    Physical extents    14848 to 20478
```

## Conversion Tasks

### Convert QCOW2 to LVM
```
qemu-img convert disk0.qcow2 -O raw disk0.raw

sudo dd if=disk.raw of=/dev/vgroup/lv_disk
```

## SSD Caching
In this example: adding multiple caches on an SSD (encrypted via LUKS) to a VG on a slow array (4 x 4TB HDDs) with pre-existing LVs.

The SSD is `/dev/mapper/ssd` and the VG is `vg0`.

### Create the PV on the SSD
```
pvcreate /dev/mapper/ssd
```

### Extend the original volume group
```
vgextend vg0 /dev/mapper/ssd
```

### Create LV caches
```
lvcreate -L 40G -n cache0 vg0 /dev/mapper/ssd
lvcreate -L 40G -n cache1 vg0 /dev/mapper/ssd
lvcreate -L 40G -n cache2 vg0 /dev/mapper/ssd
```

### Add the cache to the existing logical volumes
```
lvconvert --type cache --cachepool vg0/cache0 vg0/lv0
lvconvert --type cache --cachepool vg0/cache1 vg0/lv1
lvconvert --type cache --cachepool vg0/cache2 vg0/lv2
```
### Check cache utilisation
You can see utilization with `lvs -a -o +devices`.

Writeback caching is not recommended unless your data is easily replaceable.

## File System Tasks

### Resize EXT4
```
sudo resize2fs /dev/Array0_Decrypted/Array0_Media
```

### Resize XFS
```
sudo xfs_growfs /dev/centos/root
```

## Quick getting started guide for people who haven't used LVM before
1. Have or add a disk to the system, use `lsblk` or similar to get its path (`/dev/sdx` used in this example)

2. Create a Physical Volume
```
pvcreate /dev/sdx
```

3. Create a Volume Group

Replace `<vgname>` with the name you want to use for your Volume Group (`vg0`, `data`, etc)

```
vgcreate <vgname> /dev/sdx
```

4. Create a Logical Volume
Replace `<lvname>` with the name you want to use for your Logical Volume. For ease I'd suggest using something like `var-lib-influxdb` if you plan to mount the LV at `/var/lib/influxdb`.

Replace `<size>` with something like `20G` or your desired size.

```
lvcreate -L <size> -n /dev/<vgname>/<lvname>
```

5. Create a file system on the LV
Using ext4 as an example:

```
mkfs.ext4 /dev/<vgname>/<lvname>
```

6. Create a new directory and mount the LV

If you are intending to replace an existing directory **skip to Step 7**.

  1. Create the new directory
```
mkdir /path/to/mount
```

  2. Update `/etc/fstab` with the new mount details
```
/dev/<vgname>/<lvname> /path/to/mount ext4 errors=remount-ro   0       2
```

  3. Mount it via the mount command to make sure fstab works
```
mount -a
```

7. Mount the LV over an existing directory

Follow this process if you're wanting to replace an existing directory with an LV, normally done if your root disk is low on space and you want to move something like `/var/log` off.

As part of this process we create a temporary directory, mount the LV to the temporary directory, move the data accross, check the permissions and then unmount.

Before doing this you should make sure you created a LV large enough to hold the data from the directory.

  1. Make a note of the permissions on the original directory

Using the example of `/path/to/mount` do an `ls -la` one directory down
```
ls -la /path/to/
```

  2. Create a temporary directory
```
mkdir /path/to/temp-temp-temp
```

  3. Mount the new LV on the temporary directory
```
mount /dev/<vgname>/<lvname> /path/to/temp-temp-temp
```

  4. **Stop any services using the directory!**
If you are moving files that are being actively used, will be automatically replaced or are required by a running process you will have an unhappy system and may lose all the data you're trying to make room for.

Some example services would be `docker` for `/var/lib/docker` or `rsyslogd` in `/var/log`. This will vary based on operating system, the version of the OS and any changes that have been made to the host since installation.

  4. Move the directory contents

If you're less confident about this and don't mind cleaning the old directory afterwards use rsync instead.

```
mv /path/to/mount/* /path/to/temp-temp-temp/
```

  5. Unmount and remove the temporary directory
```
umount /path/to/temp-temp-temp/ && rm -r /path/to/temp-temp-temp/
```

  6. Update `/etc/fstab` with the new mount details
```
/dev/<vgname>/<lvname> /path/to/mount ext4 errors=remount-ro   0       2
```

  7. Mount it via the mount command to make sure fstab works
```
mount -a
```

  8. Using the output from the `ls -la` command earlier set the appropriate user, group and permission settings on the mounted directory

For example if you are trying to relpace `/var/log/` the `ls -la` output for `/var/` is something like this:

```
$ ls -la /var
total 56
drwxr-xr-x 14 root root   4096 May 11  2021 .
drwxr-xr-x 23 root root   4096 May  6 01:38 ..
drwxr-xr-x  2 root root   4096 May  8 06:25 backups
drwxr-xr-x 10 root root   4096 May 31  2021 cache
drwxrwxrwt  2 root root   4096 Jan 26  2019 crash
drwxr-xr-x 44 root root   4096 Apr 24 06:13 lib
drwxrwsr-x  2 root staff  4096 Apr 24  2018 local
lrwxrwxrwx  1 root root      9 Jan 26  2019 lock -> /run/lock
drwxrwxr-x 12 root syslog 4096 May  8 06:25 log
drwxrwsr-x  2 root mail   4096 Jan 26  2019 mail
drwxr-xr-x  2 root root   4096 Jan 26  2019 opt
drwxr-xr-x  3 root root   4096 May  6 01:38 osquery
lrwxrwxrwx  1 root root      4 Jan 26  2019 run -> /run
drwxr-xr-x  2 root root   4096 Jul 19  2018 snap
drwxr-xr-x  4 root root   4096 Jan 26  2019 spool
drwxrwxrwt  6 root root   4096 May  8 21:17 tmp
```

So you would want to make `/var/log` owned by root, with the group root and with `drwxrwxr-x` permissions. **This will vary with each directory, please don't assume these are the right permissions** - that's why I haven't put the commands here.

  9. Start any services you stopped that were using the directory

If they start fine I'd then recommend restarting the system to make sure it survives a reboot.