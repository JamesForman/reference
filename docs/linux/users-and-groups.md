
## Manually create user
```
sudo useradd user1 -s /bin/bash --create-home --home-dir /home/user1
sudo adduser user2 --shell /bin/bash --home /home/user2
```

## Add user to group ==
```
sudo usermod -a -G groupname username
```

## Remove user from group
```
sudo deluser username groupname
sudo groupadd -g 10000 groupname
sudo groupadd groupname
```

