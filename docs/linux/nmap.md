`nmap -p 22 --open -sV 10.0.0.0/24`

`-p 22`: specifies the port to test

`--open`: suppress output for clients that are not listening

`-sV`: display the version string reported by the scanned server 
