


### Parse a Certificate

```
openssl x509 -text -noout -in file.pem
```

### Parse a CRL

```
openssl crl -inform DER -text -noout -in mycrl.crl
```

