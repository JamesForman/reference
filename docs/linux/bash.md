
### Loop through a file

```
while read p; do
  echo "$p"
done <peptides.txt
```

or

```
cat peptides.txt | while read line 
do
   # do something with $line here
done
```

### Loop through X

For example to make sure DNS results are the same over multiple name servers:

```
$ for i in {1..4}; do dig -t a +short server.example.com @ns${i}.example.com; done
10.129.13.107
10.129.13.107
10.129.13.107
10.129.13.107

$ for i in {1..2}; do dig -t a +short server.example.com @ns${i}.example.com; done
10.129.13.107
10.129.13.107
```
