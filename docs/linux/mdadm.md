
### Create an Array

#### Mirror Boot while installing Ubuntu Server

See the ubuntu-os-specific.md file.

#### Mirror

`sudo mdadm --create --verbose /dev/md0 --level=mirror --raid-devices=2 /dev/sdc /dev/sdd`

### Modify an Array

#### Add a new disk to an array

```
mdadm --manage /dev/md127 --add /dev/sde
mdadm: added /dev/sde
```

#### Growing an array

`mdadm --grow --raid-devices=4 /dev/md1`

#### Read Only / Read Write

```
cat /proc/mdstat 
Personalities : [raid1] [raid6] [raid5] [raid4] [linear] [multipath] [raid0] [raid10] 
md127 : active (auto-read-only) raid6 sdk1[1] sdf[7] sdj[4] sdi[9] sdd[8] sdg[10] sdh[6]
      17580801024 blocks super 1.2 level 6, 512k chunk, algorithm 2 [8/7] [_UUUUUUU]
```

Run: `mdadm --readwrite /dev/md127`

```
cat /proc/mdstat 
Personalities : [raid1] [raid6] [raid5] [raid4] [linear] [multipath] [raid0] [raid10] 
md127 : active raid6 sdk1[1] sdf[7] sdj[4] sdi[9] sdd[8] sdg[10] sdh[6]
      17580801024 blocks super 1.2 level 6, 512k chunk, algorithm 2 [8/7] [_UUUUUUU]
```

#### Convert RAID6 to RAID5

```
sudo mdadm --grow /dev/md127 --level=raid5 --raid-devices=8
mdadm: Need 1 spare to avoid degraded array, and only have 0.
       Use --force to over-ride this check.
```

```
admin@example-server:~⟫ sudo mdadm --grow /dev/md127 --level=raid5 --raid-devices=7
raid_disks for /dev/md127 set to 7
```

### Tweaks and other bits

#### Convincing the OS to not call an array md127
Update `/etc/mdadm/mdadm.conf` with the array details.

The fast way:

`mdadm --detail --scan >> /etc/mdadm/mdadm.conf`

Be warned that this is likely to remove options such as `MAILADDR root`.

Once the mdadm.conf file is updated you need to run `sudo update-initramfs -u`.

#### Make recovery faster

```
sudo sysctl -w dev.raid.speed_limit_max=100000
sudo sysctl -w dev.raid.speed_limit_min=90000
```

#### Duplicate a partition table
For when you're replacing an OS drive with grub/etc.

`sfdisk -d /dev/sda | sfdisk /dev/sdb`

### Remove an array

```
mdadm --stop /dev/md0
mdadm --remove /dev/md0
mdadm --zero-superblock /dev/sda
```

Repeat zero-superblock for each device that was in the array.