## journalctl

### Follow and Recent
Follow:

`journalctl -f`

X most recent lines:

`journalctl -n 50`

### Vacuum
Remove logs older than X days:

`journalctl --vacuum-time=28d`

### Limit to boot
Limit to the current boot:

`journalctl -b`

Limit to a previous boot:

`journalctl -b -1`

List the boots:

`journalctl --list-boots`

### Time Ranges
Last hour:

`journalctl --since "1 hour ago"`

### By service:
Single:

`journalctl -u nginx.service`

Multiple:

`journalctl -u nginx.service -u mysql.service`

### By User
Get id:

```
$id mysql

uid=108(mysql) gid=116(mysql) groups=116(mysql)
```

Logs:

`journalctl _UID=108`

### By Priority
Logs from the last boot between emergency and critical:

`journalctl -b -1  -p "emerg".."crit"`
