## Search and replace

### Search
 `:s/string`

### Replace
 `:s/pattern/replace`

### Replace EVERY OCCURANCE
 `:%s/pattern/replace`

## Delete ===

### From current line to end of file
`d G`

### From current cursor to the end of file
`d Ctrl+End`

