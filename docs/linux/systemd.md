
## Failed units

```
systemctl --failed
```

or

```
systemctl list-units --state=failed
```
