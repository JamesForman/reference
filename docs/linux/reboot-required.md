`/var/run/reboot-required`

```bash
file /var/run/reboot-required
stat /var/run/reboot-required
ls /var/run/reboot-required
```

```bash
#!/bin/bash
if [ -f /var/run/reboot-required ]; then
bash
  echo 'reboot required'
fi
```

```ma-t
alias rr='if [ -f /var/run/reboot-required ]; then echo "reboot required"; else echo "No reboot needed"; fi'
```

The file `/var/run/reboot-required.pkgs` will list the packages that require the reboot.
