```
I often find myself consecutively filtering with multiple chained greps

grep a input | grep b | grep -v c | grep d

and found it was easier & cleaner to do in one awk expression

awk '/a/ && /b/ && !/c/ && /d/' input

Just in case someone else finds it useful.
```

https://twitter.com/gumnos/status/1005823998796730369

