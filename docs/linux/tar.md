# Tar

## Plain

`tar -cf tarfile.tar /directory/to/tar`

`tar -xf tarfile.tar /directory/to/untar`

## BZ2

`tar -cjf tarfile.tar.bz2 /directory/to/tar`

`tar -xjf tarfile.tar /directory/to/untar`
