### Installing Ubuntu Desktop on Ubuntu Server

`sudo apt install ubuntu-desktop`

### Using the installer to setup RAID1 (mirrored) boot disks

- Under Guided storage configuration, select Custom storage layout and hit the space bar
  - Select Done and hit enter

- Storage configuration
  - Clear all partitions from your drives (if existing partitions/RAID exist)
  - You should now have 2 identically-sized drives with all free space under AVAILABLE DEVICES
    - For each of your drives under AVAILABLE DEVICES...
      - Tab to select the local disk and hit enter
      - Tab to select Use As Boot Device (or Add As Another Boot Device) and hit enter
      - Tab- to select free space under that drive and hit enter
      - Tab- to select  Add GPT Partition and hit enter
          - Tab, then hit enter
          - Tab to select Leave unformatted, then hit enter
          - Tab to select Create then hit enter
    - Select Create software RAID (md) and hit enter
      - Leave Name as md0 and tab
      - Leave RAID Level as 1 (mirrored) and tab
      - Tab to the first of you drives' partitions labeled as partition 2 and hit Space to choose it
      - Tab to the second of you drives' partitions labeled as partition 2 and hit Space to choose it
      - Tab to Create and hit enter
    - Under md0 select free space and hit enter
    - Select  Add GPT Partition and hit enter
      - Set the Size to 2G and hit tab
      - Tab again, leaving Format set to ext4
      - On Mount, hit enter, then select /boot, then hit enter
      - Tab to select Create then hit enter
    - Under md0 select free space again and hit enter
    - Select  Add GPT Partition and hit enter
      - Leave the Size blank and hit tab
      - Hit enter on Format, select Leave unformatted, and hit enter
      - Tab to select Create then hit enter
    - Select Create volume group (LVM) and hit enter
      - Leave Name set to vg0 and hit tab
      - Under md0 select partition 2 and hit space to choose it
      - Tab to Create and hit enter
    - Under vg0 (new) select free space and hit enter
    - Select Create Logical Volume and hit enter
      - Leave the Name set to lv-0
      - Select Create and hit enter
    - Select Done and hit enter
    - On the Confirm destructive action screen, select Continue and hit enter

[Source](https://www.muchtall.com/2023/03/10/setting-up-and-ubuntu-22-04-server-with-raid1-and-lvm/).