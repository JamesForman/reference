
### Reserved space
See how much is set
```
sudo tune2fs -l /dev/sdb1 | grep ‘Reserved block count’
```

Change the amount set
```
sudo tune2fs -m 0 /dev/sdb1
```

