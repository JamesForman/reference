## Allow access to a port from a specific IP

`sudo ufw allow from 192.168.1.1 to any port 5665`
