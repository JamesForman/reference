Reference: https://www.thegeekdiary.com/understanding-the-job-control-commands-in-linux-bg-fg-and-ctrlz/

## What is a job?
A job is a process that the shell manages. Each job is assigned a sequential job ID. Because a job is a process, each job has an associated PID. There are three types of job statuses:

### Foreground
When you enter a command in a terminal window, the command occupies that terminal window until it completes. This is a foreground job.

### Background
When you enter an ampersand (&) symbol at the end of a command line, the command runs without occupying the terminal window. The shell prompt is displayed immediately after you press Return. This is an example of a background job.

### Stopped
If you press Control + Z for a foreground job, or enter the stop command for a background job, the job stops. This job is called a stopped job.

## Commands
Job control commands enable you to place jobs in the foreground or background, and to start or stop jobs.

**Note**: The job control commands enable you to run and manage multiple jobs within a shell. However, you can use the job control commands **only** in the shell where the job was initiated.

| Command | Description |
| ------ | ------ |
| `jobs` | List all jobs |
| `bg <number>` | Places the job (numbered) in the background |
| `fg <number>` | Brings the current or specified job into the foreground |
| ctrl+z | Stops the foreground job and places it in the background as a stopped job |


