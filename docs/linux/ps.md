### Which process is eating all the RAMs?

```
ps -e -o pid,vsz,comm= | sort -n -k 2
```
