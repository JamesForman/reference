`strace -p $PID -f`

`-f`: Follow forks / child procs 


`strace -yy -y -tt -T -s512`

* Gives file names and longer strings
* `-yy` - Print all available information associated with file descriptors: protocol-specific information associated with socket file descriptors, block/character device number associated with device file descriptors, and PIDs associated with pidfd file descriptors.
* `-y` - Print paths associated with file descriptor arguments and with the AT_FDCWD constant
* `-tt` - If given twice, the time printed will include the microseconds.
* `-T` - Show the time spent in system calls.  This records the time difference between the beginning and the end of each system call.  precision can be one of s (for seconds), ms (milliseconds), us (microseconds), or ns (nanoseconds), and allows setting the precision of time value being printed.  Default is us (microseconds).
* `-s512` - Specify the maximum string size to print (the default is 32).  Note that filenames are not considered strings and are always printed in full.


## Trace based on process name
Useful for processes that are dying very quickly or in restart loops.

`ps auxw | grep [PROCESS-NAME] | awk '{print"-p " $2}' | xargs strace`
