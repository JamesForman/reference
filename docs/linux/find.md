### Delete files in folder matching name sess_* older than 7 days:

```
find /srv/sitedata/wordpress/oompher/phpsessions/ -type f -mtime +7 -name 'sess_*' -execdir rm -- {} \;
```

### Delete files modified more than 5 days ago

```
find /path/to/files/ -mtime +5 -exec rm {} \;
```

### Move files smaller than X size

```
find . -type f -maxdepth 1 -size -50k -exec mv {} temp/ \;
```

