
### Logging console output to a file
Normal:

```
    <serial type='pty'>
      <target port='0'/>
    </serial>
    <console type='pty'>
      <target type='serial' port='0'/>
    </console>
```

Log to file:

```
    <serial type='file'>
      <source path='/var/log/libvirt/qemu/example-server-serial.log'/>
      <target port='0'/>
    </serial>
    <console type='file'>
      <source path='/var/log/libvirt/qemu/example-server-serial.log'/>
      <target type='serial' port='0'/>
    </console>
```

### Reload XML file

```
virsh define foo.xml
```

### Stop and Start ASAP

```
virsh shutdown example-server; while true; do virsh start example-server && break; sleep 1; done
```

