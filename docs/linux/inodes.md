
### Find files using the most inodes

```
sudo find /var/www -xdev -printf '%h\n' | sort | uniq -c | sort -k 1 -n
```