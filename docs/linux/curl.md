
## Checking Redirects

```
server:~ user$ curl -L test.example.com 
curl: (6) Could not resolve host: alternate.example.com
```

```
server:~ user$ curl -Lv test.example.com 
* Rebuilt URL to: test.example.com/
*   Trying 192.168.1.5...
* TCP_NODELAY set
* Connected to test.example.com (192.168.1.5) port 80 (#0)
> GET / HTTP/1.1
> Host: test.example.com
> User-Agent: curl/7.51.0
> Accept: */*
> 
< HTTP/1.1 302 Moved Temporarily
< Server: nginx/1.10.3 (Ubuntu)
< Date: Sat, 27 May 2017 01:39:47 GMT
< Content-Type: text/html
< Content-Length: 170
< Connection: keep-alive
< Location: https://alternate.example.com/
< 
* Ignoring the response-body
* Curl_http_done: called premature == 0
* Connection #0 to host test.example.com left intact
* Issue another request to this URL: 'https://alternate.example.com/'
* Could not resolve host: alternate.example.com
* Closing connection 1
curl: (6) Could not resolve host: alternate.example.com
```

