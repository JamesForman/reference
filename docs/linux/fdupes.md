

### View a list of duplicates
```
fudpes --recurse .
```

### Remove all but one duplicate automatically
```
fdupes --recurse --omitfirst --delete --noprompt .
```