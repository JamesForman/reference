See also: https://wiki.ubuntu.com/Releases


| Version | Code Name             | LTS | Support Status                        |
| ------- | --------------------- | --- | ------------------------------------- |
| 24.04   | **Noble** Numbat      | Yes | Ends June 2029 (ESM until April 2036) |
| 23.10   | **Mantic** Minotaur   | No  | Ends July 2024                        |
| 23.04   | **Lunar** Lobster     | No  | EOL                                   |
| 22.10   | **Kinetic** Kudu      | No  | EOL                                   |
| 22.04   | **Jammy** Jellyfish   | Yes | Ends June 2027 (ESM until April 2032) |
| 21.10   | **Impish** Indri      | No  | EOL                                   |
| 21.04   | **Hirsute** Hippo     | No  | EOL                                   |
| 20.10   | **Groovy** Gorilla    | No  | EOL                                   |
| 20.04   | **Focal** Fossa       | Yes | ESM until April 2030 |
| 19.10   | **Eoan** Ermine       | No  | EOL                                   |
| 19.04   | **Disco** Dingo       | No  | EOL                                   |
| 18.10   | **Cosmic** Cuttlefish | No  | EOL                                   |
| 18.04   | **Bionic** Beaver     | Yes | EOL (ESM until April 2028) |
| 17.10   | **Artful** Aardvark   | No  | EOL                                   |
| 17.04   | **Zesty** Zapus       | No  | EOL                                   |
| 16.10   | **Yakkety** Yak       | No  | EOL                                   |
| 16.04   | **Xenial** Xerus      | Yes | EOL (ESM until April 2026) |
| 15.10   | **Wily** Werewolf     | No  | EOL                                   |
| 15.04   | **Vivid** Vervet      | No  | EOL                                   |
| 14.10   | **Utopic** Unicorn    | No  | EOL                                   |
| 14.04   | **Trusty** Tahr       | Yes | EOL (ESM until April 2024) |
| 13.10   | **Saucy** Salamander  | No  | EOL                                   |
| 13.04   | **Raring** Ringtail   | No  | EOL                                   |
| 12.10   | **Quantal** Quetzal   | No  | EOL                                   |
| 12.04   | **Precise** Pangolin  | Yes | EOL                                   |
| 11.10   | **Oneiric** Ocelot    | No  | EOL                                   |
| 11.04   | **Natty** Narwhal     | No  | EOL                                   |
| 10.10   | **Maverick** Meerkat  | No  | EOL                                   |
| 10.04   | **Lucid** Lynx        | Yes | EOL                                   |
| 9.10    | **Karmic** Koala      | No  | EOL                                   |
| 9.04    | **Jaunty** Jackalope  | No  | EOL                                   |
| 8.10    | **Intrepid** Ibex     | No  | EOL                                   |
| 8.04    | **Hardy** Heron       | Yes | EOL                                   |
| 7.10    | **Gusty** Gibbon      | No  | EOL                                   |
| 7.04    | **Fiesty** Fawn       | No  | EOL                                   |
| 6.10    | **Edgy** Eft          | No  | EOL                                   |
| 6.06    | **Dapper** Drake      | Yes | EOL                                   |
| 5.10    | **Breezy** Badger     | No  | EOL                                   |
| 5.04    | **Hoary** Hedgehog    | No  | EOL                                   |
| 4.10    | **Warty** Warthog     | No  | EOL                                   |


Older repositories are available via http://old-releases.ubuntu.com/releases
