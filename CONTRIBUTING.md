# Contributing

It is our goal for this repository to be accessible to everyone and we welcome positive contributions.

## New contributors

Hello and welcome! :)

We know that making your first contribution to a project can be a little bit scary and we’d like to help with that. If you’re concerned at all please feel free to reach out to us through a confidential issue and we will help you.

## Code of Conduct

All contributors are expected to follow our code of conduct.

## How to contribute

You can contribute by:

* Creating a branch
* Commit and push your updates to the branch
* Create a Merge Request

If you have questions or concerns you're most welcome to create an issue asking about them prior to or associated with your merge request.

## Convention and style guide

GitLab CI runs various tests on all merge requests, including a spell check, linting of markdown and [alex](https://alexjs.com/) to find any gender favouring, polarising, race related, religion inconsiderate, or other unequal phrasing in the content.

## Recognition

It is important that Open Source contributions are recognised. Please update the contributors file in the project as part of your merge requests to include your desired name.
